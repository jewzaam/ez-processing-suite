$DeployTarget = '..\..\'
$TemplateName = "updates_template.xri"

Copy-Item ..\RistaMask_UI.js "$DeployTarget\src\scripts"
Copy-Item ..\EZ_Decon.js "$DeployTarget\src\scripts"
Copy-Item ..\EZ_StarReduction.js "$DeployTarget\src\scripts"
Copy-Item ..\EZ_SoftStretch.js "$DeployTarget\src\scripts"
Copy-Item ..\EZ_LiveStack.js "$DeployTarget\src\scripts"
Copy-Item ..\EZ_HDR.js "$DeployTarget\src\scripts"
Copy-Item ..\EZ_Common.js "$DeployTarget\src\scripts"
tar -cvzf "$DeployTarget\release.tar.gz" -C "$DeployTarget" "src"
$Hash = Get-FileHash "$DeployTarget\release.tar.gz" -Algorithm SHA1 | Select -ExpandProperty Hash
$Hash = $Hash.toLower()
Copy-Item "$TemplateName" "$DeployTarget\updates.xri"

((Get-Content -path "$DeployTarget\updates.xri" -Raw) -replace 'HASH',$hash.ToString()) | Set-Content -Path "$DeployTarget\updates.xri"
$releaseDateString = 'releaseDate="' + $(Get-Date -Format "yyyyMMddHHmmss") + '"'
((Get-Content -path "$DeployTarget\updates.xri" -Raw) -replace 'releaseDate="DATE"',$releaseDateString) | Set-Content -Path "$DeployTarget\updates.xri"