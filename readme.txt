How to install the script:

1. Download the .js file

2. Copy the file to .../PixInsight/src/scripts

3. Open PixInsight and go to Scripts -> Feature Scripts

4. Click on "Add" and navigate to the directory you copied the script to

5. It should tell you that it found a new script. Now you can access it from Scripts -> Noise Reduction